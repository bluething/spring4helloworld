/**
 * 
 */
package com.pegipegi.core;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class HelloWorld {
	
	private String name;

	public void setName(String name) {
		this.name = name;
	}
	
	public void printHello() {
		System.out.println("Spring 4: Hello!" + name);
	}

}
